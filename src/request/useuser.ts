import requset from '../until/request.ts'
const useuser=(userid:any,token:any)=>requset.get('/api/user/info',{
    params:{
        userid
    },
    headers: {
        'token': `${token}`,
        'X-Requested-With': 'XMLHttpRequest'
      }
})
export default useuser