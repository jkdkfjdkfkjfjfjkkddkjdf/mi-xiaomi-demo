import requset from '../until/request.ts'
//验证手机号
export const VerifyYourPhoneNumber=(tel:any)=>requset.post('/api/user/docheckphone',{
        tel
})
//发送验证码
export const SendAerificationCode=(tel:any)=>requset.post('/api/user/dosendmsgcode',{
        tel
})
//验证验证码是否正确
export const  yzSendAerificationCode=(tel:any,telcode:any)=>requset.post('/api/user/docheckcode',{
        tel,
        telcode
})
//注册
export const zhuce=(tel:any,password:any)=>requset.post('/api/user/dofinishregister',{
     tel,
     password
})