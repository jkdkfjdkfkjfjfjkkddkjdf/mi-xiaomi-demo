import requset from '../until/request.ts'
/**
 * 
 * @returns 
 * 分类列表
 */
export const fenlei=()=>requset.get('/api/pro/categorylist')
/**
 * 
 * @param category 
 * @returns 
 * 分类页面下的品牌列表
 */
export const pingpai=(category:any)=>requset.get('/api/pro/categorybrandlist',{
    params:{
        category
    }
})
export const list=(count=1,limitNum=10,category:any,brand:any)=>requset.get('/api/pro/categorybrandprolist',{
    params:{
         count,
        limitNum,
        category,
        brand
    }
})