import {defineStore} from 'pinia'
let pinia=defineStore('cart',{
   state:()=>({
     cart:[]
   }),
   getters:{

   },
   actions:{
    add(poid:any){
        this.cart.push(poid)
    }
   }
})
export default pinia