import {type RouteRecordRaw} from "vue-router"
import Home from '../views/home/index.vue'
import fen from '../views/fenlei/index.vue'
import cart from '../views/cart/index.vue'
import mine from '../views/mine/index.vue'
import navber from '../components/navbar.vue'
import taber from '../components/taber.vue'
import datail from '../views/detail/index.vue'
import fenlei from '../views/fenlei/fenleixiangqing.vue'
import login from '../views/login/index.vue'
import user from '../views/login/user.vue'
const router:RouteRecordRaw[]=[
   {
   path:'/',
   components:{
    footer:taber,
    default:Home
   },
   meta:{
    icon:'wap-home',
    title:'首页',
    inTaber:true
   }
   },
   {
    path:'/fen',
    components:{
        header:navber,
        footer:taber,
        default:fen
    },
    meta:{
        icon:'bookmark',
        title:'分类',
        inTaber:true
    }
   },
   {
    path:'/cart',
    components:{
        header:navber,
        footer:taber,
        default:cart
    },
    meta:{
        icon:'cart',
        title:'购物车',
        inTaber:true
    }
},
    {
        path:'/mine',
        components:{
            header:navber,
            footer:taber,
            default:mine
        },
        meta:{
            icon:'manager',
            title:'我的',
            inTaber:true
        }
    },
    {
        path:'/datail',
        components:{
            header:navber,
            default:datail
        },
        meta:{
            title:'该商品详情页面'
        }
    },
    {
        path:'/fenlei',  
        components:{
            header:navber,
            default:fenlei
        },
        meta:{
            title:'该商品详情'
        }      
    },
    {
        path:'/login',
        components:{
            header:navber,
            default:login
        },
        meta:{
            title:'登录页面'
        }
    },
    {
        path:'/user',
        components:{
            header:navber,
            default:user
        },
        meta:{
            title:'注册页面'
        }
    },
]
export default router