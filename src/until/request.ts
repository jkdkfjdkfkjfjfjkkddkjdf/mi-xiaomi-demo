import axios from 'axios'
import { showToast, closeToast } from 'vant'

// 创建 axios 实例
const service = axios.create({
  baseURL: 'http://162.14.108.31:3001', // 所有接口的基准 url 地址
  timeout: 10000, // 超时时间
})

// 请求拦截
service.interceptors.request.use(
  (config) => {
    // 提示请求数据加载中...
    showToast({
      type: 'loading',
      message: '加载中...',
      duration: 0, // 设置为0，则轻提示框不会自动关闭，需要后期手动关闭
    })

    // 其它请求拦截业务
    // TODO...

    return config
  },
  (err) => {
    // 请求拦截异常
    showToast({
      type: 'fail',
      message: '请求拦截异常:' + err,
    })
  }
)

// 响应拦截
service.interceptors.response.use(
  (response) => {
    // 关闭数据加载轻提示效果
    closeToast(true)

    // response 是 axios 对服务端接口响应的一个包装对象
    // status 属性表示http响应状态码
    // data 属性表示后端向前端响应返回的主体数据
    return response.data
  },
  (err) => {
    // 响应拦截异常
    showToast({
      type: 'fail',
      message: '响应拦截异常:' + err,
    })
  }
)

export default service